﻿using Android.App;
using Android.Content;
using Firebase;
using Firebase.Database;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Models;

namespace ToDo.Repositories
{
    internal sealed class DataRepository : IDataRepository
    {
        private FirebaseClient db;

        public DataRepository()
        {
            db = new FirebaseClient($"https://todo-d9150-default-rtdb.europe-west1.firebasedatabase.app/{Parameters.UserId}");
        }

        public async Task Add(string id, Item item)
        {

            var json = JsonConvert.SerializeObject(item);
            await db.Child(id).PutAsync(json);
        }

        public async Task<Item> Get(string id)
        {
            return await db.Child(id).OnceSingleAsync<Item>();
        }

        public async Task<List<(string, Item)>> GetAll()
        { 
            var rootDb = new FirebaseClient("https://todo-d9150-default-rtdb.europe-west1.firebasedatabase.app/");
            var firebaseObject = await rootDb.Child(Parameters.UserId).OnceAsync<Item>();
            return firebaseObject.Select(x => (x.Key, x.Object)).ToList();
        }

        public async Task Update(string id, Item item)
        {
            var json = JsonConvert.SerializeObject(item);
            await db.Child(id).PutAsync(json);
        }

        public async Task Delete(string id)
        {
            await db.Child(id).DeleteAsync();
        }
    }
}