﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDo.Models;

namespace ToDo.Repositories
{
    internal interface IDataRepository
    {
        Task Add(string id, Item item);

        Task Update(string id, Item item);

        Task<List<(string, Item)>> GetAll();

        Task<Item> Get(string id);

        Task Delete(string id);
    }
}