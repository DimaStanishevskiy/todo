﻿using Firebase.Auth;
using System.Threading.Tasks;

namespace ToDo.Services
{
    internal interface IAuthService
    {
        Task<FirebaseAuthLink> Login(string email, string password);

        Task<FirebaseAuthLink> Register(string email, string password);
    }
}