﻿using Firebase.Auth;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDo.Models;
using ToDo.Repositories;

namespace ToDo.Services
{
    internal sealed class DataService : IDataService
    {
        private IDataRepository dataRepository;


        public DataService()
        {
            dataRepository = new DataRepository();
        }

        public async Task Add(string header, string description)
        {
            var id = Guid.NewGuid().ToString();
            var item = new Item
            {
                Header = header,
                Description = description,
            };

            await dataRepository.Add(id, item);
        }

        public async Task<Item> Get(string id)
        {
            return await dataRepository.Get(id);
        }

        public async Task<List<(string Id, Item Item)>> GetAll()
        {
            return await dataRepository.GetAll();
        }

        public async Task Update(string id, string header, string description)
        {
            var item = new Item
            {
                Header = header,
                Description = description,
            };

            await dataRepository.Add(id, item);
        }

        public async Task Delete(string id)
        {
            await dataRepository.Delete(id);
        }
    }
}