﻿using Firebase.Auth;
using System.Threading.Tasks;

namespace ToDo.Services
{
    internal sealed class AuthService : IAuthService
    {
        private FirebaseAuthProvider authProvider;

        public AuthService()
        {
            var config = new FirebaseConfig("AIzaSyA-MDbkNhQRXfZjHPCwFseBDenW538K5Xo");

            authProvider = new FirebaseAuthProvider(config);
        }


        public async Task<FirebaseAuthLink> Login(string email, string password)
        {
            return await authProvider.SignInWithEmailAndPasswordAsync(email, password);
        }

        public async Task<FirebaseAuthLink> Register(string email, string password)
        {
            return await authProvider.CreateUserWithEmailAndPasswordAsync(email, password);
        }
    }
}