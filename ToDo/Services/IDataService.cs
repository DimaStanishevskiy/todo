﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDo.Models;

namespace ToDo.Services
{
    internal interface IDataService
    {
        Task Add(string header, string description);

        Task Update(string id, string header, string description);

        Task<List<(string Id, Item Item)>> GetAll();

        Task<Item> Get(string id);

        Task Delete(string id);
    }
}