﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using ToDo.Services;

namespace ToDo
{
    [Activity(Label = "ListActivity")]
    public class ListActivity : Activity
    {
        private IDataService dataService;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            dataService = new DataService();

            SetContentView(Resource.Layout.activity_list);

            await AddItems();

            var button = FindViewById<Button>(Resource.Id.add);
            button.Click += (obj, ev) => CreateNew();
        }

        private async Task AddItems()
        {
            var items = await dataService.GetAll();

            var listLayout = FindViewById<LinearLayout>(Resource.Id.listLayout);

            foreach (var item in items)
            {
                var button = new Button(this);
                button.Text = item.Item.Header;
                button.SetBackgroundColor(Color.White);
                button.Click += (obj, ev) => Update(item.Id);

                listLayout.AddView(button);
            }
        }

        private void CreateNew()
        {
            Intent intent = new Intent(this, typeof(NewItemActivity));
            StartActivity(intent);
        }

        private void Update(string id)
        {
            Intent intent = new Intent(this, typeof(ItemActivity));
            intent.PutExtra("id", id);
            StartActivity(intent);
        }
    }
}