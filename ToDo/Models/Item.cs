﻿
namespace ToDo.Models
{
    internal sealed class Item
    {
        public string Header { get; set; }

        public string Description { get; set; }
    }
}