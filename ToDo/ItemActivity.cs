﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using ToDo.Services;

namespace ToDo
{
    [Activity(Label = "ItemActivity")]
    public class ItemActivity : Activity
    {
        private IDataService dataService;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            dataService = new DataService();

            SetContentView(Resource.Layout.item);

            var id = Intent.GetStringExtra("id");

            var item = await dataService.Get(id);

            FindViewById<EditText>(Resource.Id.header).Text = item.Header;
            FindViewById<EditText>(Resource.Id.description).Text = item.Description;
            FindViewById<Button>(Resource.Id.update).Click += (obj, ev) => Update(id);
            FindViewById<Button>(Resource.Id.back).Click += (obj, ev) => Back();
            FindViewById<Button>(Resource.Id.delete).Click += (obj, ev) => Delete(id);
        }

        private async void Update(string id)
        {
            await dataService.Update(
                id,
                FindViewById<EditText>(Resource.Id.header).Text,
                FindViewById<EditText>(Resource.Id.description).Text
                );

            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }

        private void Back()
        {
            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }

        private async void Delete(string id)
        {
            await dataService.Delete(id);

            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }
    }
}