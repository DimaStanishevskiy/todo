﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using ToDo.Services;

namespace ToDo
{
    [Activity(Label = "NewItemActivity")]
    public class NewItemActivity : Activity
    {
        private IDataService dataService;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            dataService = new DataService();

            SetContentView(Resource.Layout.item);

            FindViewById<Button>(Resource.Id.update).Click += (obj, ev) => Add();
            FindViewById<Button>(Resource.Id.back).Click += (obj, ev) => Back();
            FindViewById<Button>(Resource.Id.delete).Enabled = false;
        }

        private async void Add()
        {
            await dataService.Add(
                FindViewById<EditText>(Resource.Id.header).Text,
                FindViewById<EditText>(Resource.Id.description).Text
                );

            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }

        private void Back()
        {
            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }
    }
}