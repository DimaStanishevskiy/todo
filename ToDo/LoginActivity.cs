﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using System;
using System.Threading.Tasks;
using ToDo.Services;

namespace ToDo
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class LoginActivity : AppCompatActivity
    {

        private IAuthService authService;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            authService = new AuthService();

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_login);

            var loginButton = FindViewById<Button>(Resource.Id.login);
            loginButton.Click += Login;

            var registerButton = FindViewById<Button>(Resource.Id.register);
            registerButton.Click += Register;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public async void Login(object sender, EventArgs e)
        {
            var email = FindViewById<EditText>(Resource.Id.email);
            var password = FindViewById<EditText>(Resource.Id.password);

            try
            {
                var result = await authService.Login(email.Text, password.Text);
                Parameters.UserId = result.User.LocalId;
            }
            catch
            {
                var errorMessage = FindViewById<TextView>(Resource.Id.error_message);
                errorMessage.Text = "Login failed";
                return;
            }

            Intent intent = new Intent(this, typeof(ListActivity));
            StartActivity(intent);
        }

        public async void Register(object sender, EventArgs e)
        {
            var email = FindViewById<EditText>(Resource.Id.email);
            var password = FindViewById<EditText>(Resource.Id.password);
            try
            {
                await authService.Register(email.Text, password.Text);
            }
            catch
            {
                var errorMessage = FindViewById<TextView>(Resource.Id.error_message);
                errorMessage.Text = "Login failed";
                return;
            }

            Login(sender, e);
        }
    }
}